/*===============================

Photons
by Guillermo López
Site: virtualclover.com.mx

=================================*/
//Navbar functionality

$(".titulo-main").hover(function(){
            
            
            $(".des").toggleClass("visible");
            
            $(this).toggleClass("orange");
        });  
        
$(".titulo-nav").click(function(){
            
            $("#menu").toggleClass("visible");
            $(this).toggleClass("visible");
            
        });
        

//Expand portfolio section

$(".see-more").click(function(){
            
            $("#portfolio").toggleClass("expand");
            
        });
        
$(".about-btn").click(function(){
            
             $("#portfolio").removeClass("expand");
            
        }); //Compress the portfolio section when the about button in the nav bar is pressesd

$(".portfolio-btn").click(function(){
            
             $("#portfolio").addClass("expand");
            
        });

//Portafolio photo page scripts
        
$(".port-img").click(function(){
            
            $(".external-pc").css("top","0vh");
            $("body").css("overflow-y","hidden");
            
            $(".photo-half .photo").attr("src",$("img",this).attr("src"));
            $(".p-desc h3").text($(".img-title",this).text());
            $(".p-desc p").text($(".desc",this).text());
            $(".inner-circles .speed a").text($(".speed",this).text());
             $(".inner-circles .ap a").text($(".ap",this).text());
             $(".inner-circles .iso a").text($(".iso",this).text());
            $(".dials").fadeIn("slow");

            
        }); //Gather the info from .port-img to display in the page
        
$(".photo-half").click(function(){
            
            $(this).toggleClass("zoom");
        });
        
$(".close-btn").click(function(){
            
            $(".external-pc").css("top","100vh");
            $(".photo-half").removeClass("zoom");
            $("body").css("overflow-y","auto");
            $(".dials").css("display","none");
        });




//Change color of navigation
        
$(window).scroll(function() {
            var position = $(window).scrollTop(); 
            var portada= $("#portada").height()-50;
            if (portada < position)
                {
                    $("nav").addClass("negro");
                } else {
                    
                    $("nav").removeClass("negro");
                }
        });



//Smooth scroll to section
        
$('a[href*="#"]').on('click', function(e) {
  e.preventDefault()

  $('html, body').animate(
    {
      scrollTop: $($(this).attr('href')).offset().top,
    },
    500,
    'linear'
  )
})
  

//Get the current year

var d = new Date();
var n = d.getFullYear();
$(".year").text(n);


//Loader
     
var frameport = $('#portada .portada-svg').drawsvg({
    
    duration: 2000
});

$(window).on('load', function()  {
	$(".loader").fadeOut("slow");
    frameport.drawsvg('animate');
    $("body").css("overflow-y","auto");
})


//Draw svg
var mySVG = $('.draw').drawsvg({
    
    duration: 300
});




$(window).scroll(function() {
            var position = $(window).scrollTop(); 
            var camarap= $("#fotones .draw").height()+150;
            if (camarap < position)
            {
                mySVG.drawsvg('animate');
                $(".draw").removeClass("draw");
                
            }});


    